<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    public $components = array( 'RequestHandler' );
    public $uses = array( 'Chatuser', 'Chatmessage', 'System' );   
    public $helpers = array( 'Formatacao' );

    /**
     * beforeFilter method
     *
     */      
    public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow( 'index', 'display', 'chat', 'close', 'status', 'listUsers', 'verifyUser', 'getUser', 'getMessage', 'saveMessage' );
	}
    
    public function index(){ 
        $this->redirect( 'display' ); 
    }
    
    /**
     * index method
     *
     */     
	public function display() 
    {      
        if( $this->Session->read( 'Auth.User.id' ) != '' ){
            $this->redirect( 'users' );
        }
        
        if( $this->request->is( 'Post' ) AND
           $this->Session->write( 'Chatuser' )
          ){      
			
            $this->request->data['Chatuser']['ip'] = $this->RequestHandler->getClientIp();        
            $this->Session->write( 'Chatuser.name', $this->request->data['Chatuser']['name'] );              
            $this->Session->write( 'Chatuser.email', $this->request->data['Chatuser']['email'] );
            $this->Chatuser->set( $this->request->data );            
            $this->Chatuser->save();
            
            $filter['conditions']['and'] = array(
                'Chatuser.name' => $this->request->data['Chatuser']['name'], 
                'Chatuser.email' => $this->request->data['Chatuser']['email'], 
                'Chatuser.ip' => $this->RequestHandler->getClientIp()
            );
            
            $user = $this->Chatuser->find( 'first', $filter );            
            $this->Session->write( 'Chatuser.id', $user['Chatuser']['id'] );    
            
            $this->redirect( 'chat' );
        }                
        
        $this->set( 'system', $this->System->find( 'first' ) );
	}
    
    
    /**
     * display method
     *
     */     
	public function chat() 
    {   
        if( $this->Session->read( 'Chatuser' ) == null ){
            $this->redirect( 'display' );
        }
	}    
    
    
    /**
     * verifyUser method
     */
    public function verifyUser()
    {
        $this->render( false, 'ajax' );
        $status = true;
        
        if( !$this->Chatuser->find( 'count', array( 'Chatuser.id' => $this->Session->read( 'Chatuser.id' )  ) ) ){
            $this->Session->destroy();
            $status = false;
        }
        
        echo json_encode( array( 'success' => $status ) );         
    }
    
    /**
     * getMessage method
     *
     */    
    public function getMessage()
    {
        $this->render( false, 'ajax' );
        
        if( !$this->Session->read( 'Chatuser.id' ) ){
            echo json_encode( 
                array( 
                    'success' => 'error'
                ) 
            );
            exit();
        }
        
        $html            = null;
        $filter['order'] = 'Chatmessage.id ASC';
        $filter['conditions']['and']['Chatmessage.chatuser_id'] = $this->Session->read( 'Chatuser.id' );
        $messages = $this->Chatmessage->find( 'all', $filter );              
        
        if ( count( $messages ) ){
            
            foreach( $messages as $message ){
                
                $user = ( $message['Chatmessage']['reply'] == 0 )
                    ? $message['Chatuser']['name'] : 'Atendente';
                $datetime = date( 'H:i:s', strtotime( $message['Chatmessage']['created'] ) );
                
                $html .= "
                    <p>
                        <u><b>{$datetime} | {$user} diz:</b></u> 
                        <i>{$message['Chatmessage']['text']}</i>
                    </p>    
                ";
            }
            
            
            echo json_encode( 
                array( 
                    'success' => true,  
                    'html' => $html 
                ) 
            );
        }else{
            echo json_encode( 
                array( 
                    'success' => false
                ) 
            );            
        }
    }
    
    
    /**
     * getUser method
     *
     */
    public function getUser()
    {
        $this->render( false, 'ajax' );
        
        $filter['conditions']['and']  = array( 'Chatuser.id' => $this->Session->read( 'Chatuser.id' ) );
        $user   =  $this->Chatuser->find( 'first', $filter );
        
        $html = "
                <a>
                    <b>Bem vindo, </b> 
                    {$user['Chatuser']['name']}
                </a>                    
        ";
        
        
        echo json_encode( array( 'success' => true, 'html' => $html ) );
    } 
    
    
    /**
     * getUser method
     *
     */
    public function adminGetUser()
    {
        $this->render( false, 'ajax' );
        
        $filter['conditions']['and']  = array( 'Chatuser.id' => $this->Session->read( 'Chatuser.id' ) );
        $user   =  $this->Chatuser->find( 'first', $filter );
        
        $html = "
                <a>
                    <b>Cliente: </b> 
                    {$user['Chatuser']['name']}
                </a>                    
        ";
        
        
        echo json_encode( array( 'success' => true, 'html' => $html ) );
    } 
    
    
    /**
     * close method
     *
     */
    public function close()
    {
        if( $this->Session->read( 'Chatuser.id' ) ){			
			if( $this->Chatmessage->delete( array( 'Chatmessage.chatuser_id' => $this->Session->read( 'Chatuser.id' ) ) ) ){
				$this->Chatuser->delete( $this->Session->read( 'Chatuser.id' ) );        	
			}            
        }
        $this->Session->destroy();
        $this->redirect( 'display' );
    }
    
    
    /**
     * adminClose method
     *
     */
    public function adminClose()
    {
        $this->redirect( 'users' );
    }    
    
    
    /**
     * saveMessage method
     *
     */
    public function saveMessage()
    {
        $this->render( false, 'ajax' );
        
        $filter['conditions']['and']['Chatmessage.text']    = $this->request->data['text'];
        $filter['conditions']['and']['Chatmessage.created'] = date( 'Y-m-d H:i:s' );     
        
        if( $this->Chatmessage->find( 'count', $filter ) == 0 ){        
            $this->request->data['Chatmessage']['chatuser_id'] = $this->Session->read( 'Chatuser.id' );
            $this->request->data['Chatmessage']['text'] = $this->request->data['text'];
            $this->Chatmessage->set( $this->request->data );
            if( $this->Chatmessage->save() ){
                echo json_encode( array( 'success' => true ) );
            }else{
                echo json_encode( array( 'success' => 'error' ) );
            }
        }
    }
    
    
    /**
     * adminSaveMessage method
     *
     */
    public function adminSaveMessage()
    {
        $filter['conditions']['and']['Chatmessage.text']    = $this->request->data['text'];
        $filter['conditions']['and']['Chatmessage.created'] = date( 'Y-m-d H:i:s' ); 
        
        if( $this->Chatmessage->find( 'count', $filter ) == 0 ){ 
            $this->render( false, 'ajax' );
            $this->request->data['Chatmessage']['chatuser_id'] = $this->Session->read( 'Chatuser.id' );
            $this->request->data['Chatmessage']['text'] = $this->request->data['text'];
            $this->request->data['Chatmessage']['reply'] = 1;
            $this->Chatmessage->set( $this->request->data );
            
            if( $this->Chatmessage->save() ){
                echo json_encode( array( 'success' => true ) );
            }else{
                echo json_encode( array( 'success' => 'error' ) );
            }
        }
    }    
    
    
    /**
     * answer method
     *
     */
    public function listUsers()
    {
        $this->layout = 'ajax';
        $this->set( 'users', $this->Chatuser->find( 'all', array( 'order' => 'id DESC' ) ) );
    }       
    
    
    /**
     * answer method
     *
     */
    public function users()
    {
        $this->set( 'users', $this->Chatuser->find( 'all', array( 'order' => 'id DESC' ) ) );
    }   

    
    /**
     * answer method
     *
     */
    public function answer( $idCliente = null )
    {
        if( $idCliente != null ){
            $this->setDataUser( $idCliente );
            $this->redirect( 'answer' );
        }        
    }    
    
    
    /**
     * answer method
     *
     */    
    private function setDataUser( $id )
    {
        $filter['conditions']['and']['Chatuser.id'] = $id;        
        $user = $this->Chatuser->find( 'first', $filter ); 
        
        $this->Session->write( 'Chatuser.name', $user['Chatuser']['name'] );           
        $this->Session->write( 'Chatuser.email', $user['Chatuser']['email'] );           
        $this->Session->write( 'Chatuser.id', $user['Chatuser']['id'] );           
        $this->Session->write( 'Chatuser.ip', $user['Chatuser']['ip'] );         
    }
    
    /**
     * deleteUser method
     *
     */   
    public function deleteUser( $id = null )
    {
        $this->Chatmessage->deleteAll( array( 'Chatmessage.chatuser_id' => $id ) );
        $this->Chatuser->deleteAll( array( 'Chatuser.id' => $id ) );  
        $this->redirect( 'users' );        
    }
    
        
    /**
     * clearAll method
     *
     */     
    public function clearAll()
    {
        $this->Chatmessage->deleteAll( array( 'Chatmessage.id > ' => 0 ) );
        $this->Chatuser->deleteAll( array( 'Chatuser.id > ' => 0 ) );  
        $this->redirect( 'users' );
    }
    
    
    /**
     * status method
     *
     */     
    public function status()
    {   
        $system = $this->System->find('first');
        
        if( $system['System']['online'] == 1 ){
            echo json_encode( array( 'success' => true, 'html' => 'Sistema online' ) );
        }else{
            echo json_encode( array( 'success' => false, 'html' => 'Sistema offline' ) );
        }
                
        $this->render( false, 'ajax' );
    }
}
