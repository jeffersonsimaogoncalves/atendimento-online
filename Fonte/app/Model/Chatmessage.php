<?php
App::uses('AppModel', 'Model');
/**
 * Chatmessage Model
 *
 * @property Chatuser $Chatuser
 */
class Chatmessage extends AppModel {

    /**
     * belongsTo associations
     *
     * @var array
     */
	public $belongsTo = array(
		'Chatuser' => array(
			'className' => 'Chatuser',
			'foreignKey' => 'chatuser_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
