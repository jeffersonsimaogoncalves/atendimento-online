<?php
App::uses('AppModel', 'Model');
/**
 * Chatuser Model
 *
 * @property Chatmessage $Chatmessage
 */
class Chatuser extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
	public $displayField = 'name';
	public $validate 	 = array(
	
		// Email do usuario
		'email' => array(
			'email' => array( 
				'rule' => array('email'),
				'message' => 'Informe um email válido' ),
				
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'O email não deve ficar sem preencher' )					
		)
    );

    /**
     * hasMany associations
     *
     * @var array
     */
	public $hasMany = array(
		'Chatmessage' => array(
			'className' => 'Chatmessage',
			'foreignKey' => 'chatuser_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
