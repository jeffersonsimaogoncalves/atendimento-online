<?php
App::uses('AppModel', 'Model');
/**
 * System Model
 *
 */
class System extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'online' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
