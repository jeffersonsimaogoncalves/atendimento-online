<?php
App::uses('Chatmessage', 'Model');

/**
 * Chatmessage Test Case
 *
 */
class ChatmessageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.chatmessage',
		'app.chatuser'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Chatmessage = ClassRegistry::init('Chatmessage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Chatmessage);

		parent::tearDown();
	}

}
