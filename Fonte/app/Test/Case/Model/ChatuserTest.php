<?php
App::uses('Chatuser', 'Model');

/**
 * Chatuser Test Case
 *
 */
class ChatuserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.chatuser'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Chatuser = ClassRegistry::init('Chatuser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Chatuser);

		parent::tearDown();
	}

}
