<script>     
    var urlbase = '<?php echo $this->Html->url( 'display' ) ?>'; 
</script>

<?php
    $this->Html->script( 
		array( 'jquery', 'ajaxGetMessageAttendant' ),
		array( 
			'inline' => false 
		) 
	);
?>

<h2>Chat Module Beta</h2>

<?php echo $this->Html->link( 'sair', $this->Html->url( 'adminClose', true ), array( 'style' => 'float: right' ) ) ?>
<br clear='both' /><br clear='both' />

<div id='chatwindow'>    
    <div id="window-users-online"></div>    
    <div id="window-conversation"></div>  
    <div id='box-message'>
        <p id='send-message' style="display: none">Enviando mensagem...</p>
    </div>    
    <input type='text' id="message" />
    <br clear='both' /><br clear='both' />
    <button>Enviar mensagem</button>
</div>

<style>
    #chatwindow{display: block; padding: 10px 0px 10px 10px; background-color: #CCC; height: 400px;}
    #window-conversation{display: block; width: 99%; height: 250px; border: 1px solid #000; background-color: #FFF; overflow: auto;}
    #window-users-online{display: block; width: 30%; background-color: #CCC; margin-bottom: 10px}
    #box-message{display: block}
    #message{width: 97%}
    buttom{margin-top: 10px}    
</style>