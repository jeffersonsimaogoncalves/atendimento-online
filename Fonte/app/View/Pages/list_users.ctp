<?php 
    $html = null;

    foreach( $users as $user): 

        $name       = $user['Chatuser']['name'];
        $email      = $user['Chatuser']['email'];
        $id         = $user['Chatuser']['id'];
        $ip         = $user['Chatuser']['ip'];
        $created    = $this->Formatacao->data( $user['Chatuser']['created'] ); 
        $countMSG   = count( $user['Chatmessage'] );
        $linkAtende = $this->Html->link(
            'Atender', 
            array(
                'action' => 'answer', 
                $user['Chatuser']['id']
            ), 
            array(
                'title' => 'Clique aqui para atender este usuário'									
            )
        );
        $linkExlui  = $this->Form->postLink(
                'Excluir', 
                array(
                    'action' => 'deleteUser', 
                    $user['Chatuser']['id']
                ), 
                array(
                    'title' => 'Clique aqui excluir este usuário'									
                ), 
                __('Você tem certeza que quer excluir este usuário? Isso poderá expulsá-lo do sistema.', 
                $user['Chatuser']['id'] ) 
        );

        $html .= <<<EOF
<tr>
    <td>$name</td>
    <td>$email</td>
    <td>$ip</td>
    <td>$created</td>
    <td>$countMSG</td>
    <td>$linkAtende  $linkExlui</td>
</tr>
EOF;
    endforeach;

    echo json_encode( array( 'success' => true, 'html' => $html ) );
?>
