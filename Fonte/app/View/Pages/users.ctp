<script> var urlbase = '<?php echo $this->Html->url( '', true ) ?>'; </script>

<?php
    
    $this->Html->script( 
		array( 'jquery', 'ajaxGetUsers' ),
		array( 
			'inline' => false 
		) 
	);
?>


<h2>Chat Module Beta</h2>

<table>
    <thead>
        <tr>
            <th>Usuário</th>
            <th>Email</th>
            <th>IP</th>
            <th>Acesso</th>
            <th>Mensagem</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="users">
        <?php foreach( $users as $user): 

            $name       = $user['Chatuser']['name'];
            $email      = $user['Chatuser']['email'];
            $id         = $user['Chatuser']['id'];
            $ip         = $user['Chatuser']['ip'];
            $created    = $this->Formatacao->data( $user['Chatuser']['created'] ); 
            $countMSG   = count( $user['Chatmessage'] );
            $linkAtende = $this->Html->link(
                'Atender', 
                array(
                    'action' => 'answer', 
                    $user['Chatuser']['id']
                ), 
                array(
                    'title' => 'Clique aqui para atender este usuário'									
                )
            );
            $linkExlui  = $this->Form->postLink(
                    'Excluir', 
                    array(
                        'action' => 'deleteUser', 
                        $user['Chatuser']['id']
                    ), 
                    array(
                        'title' => 'Clique aqui excluir este usuário'									
                    ), 
                    __('Você tem certeza que quer excluir este usuário? Isso poderá expulsá-lo do sistema.', 
                    $user['Chatuser']['id'] ) 
            );
        ?>
        
        <tr>
            <td><?php echo $name ?></td>
            <td><?php echo $email ?></td>
            <td><?php echo $ip ?></td>
            <td><?php echo $created ?></td>
            <td><?php echo $countMSG ?></td>
            <td><?php echo $linkAtende . ' ' .  $linkExlui ?></td>
        </tr>   
        <?php endforeach ?>
    </tbody>
</table>

<br clear='both' /><br clear='both' />

<?php echo $this->Html->link( 'Exlcuir todos os registros', $this->Html->url( 'clearAll', true ) ) ?>
    