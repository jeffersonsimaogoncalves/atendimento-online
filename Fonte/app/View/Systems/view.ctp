<div class="systems view">
<h2><?php echo __('System'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($system['System']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Online'); ?></dt>
		<dd>
			<?php echo h($system['System']['online']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($system['System']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($system['System']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit System'), array('action' => 'edit', $system['System']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete System'), array('action' => 'delete', $system['System']['id']), null, __('Are you sure you want to delete # %s?', $system['System']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Systems'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New System'), array('action' => 'add')); ?> </li>
	</ul>
</div>
