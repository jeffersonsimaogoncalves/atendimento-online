$(document).ready(function() {                 

    $("#message").keypress(function(e){
        if( e.which==13 ){
            if( $("#message").val() != '' ){
                saveMessage();
            }
        }
    });


    $('button').click(function(){
        if( $("#message").val() != '' ){
            saveMessage();
        }
    });
    
    // Pegando usuarios online
    $.get(
        urlbase + '/../getUser/',
        null,
        function(data){ 
             
            var retorno = jQuery.parseJSON( data );
                            
            if( retorno.success ){
                $("#window-users-online").html(retorno.html);
            }
        }
    ); 
    
    // Metodo que salva a mensagem no banco
    function saveMessage(){
        $('#send-message').show();
        $('#message, button').hide();
        
        $.post(urlbase + '/../saveMessage/', { text: $("#message").val() }, function(data) {
            $("#message").val( '' );           
        });         
    }
    
    // Pegando as mensagens 
    function getMessage(){
        
        // Verificando se nova mensagem e atualizando na tela
        $.get(
            urlbase + '/../getMessage/',
            null,
            function(data){ 
                 
                var retorno = jQuery.parseJSON( data );
                                
                if( retorno.success ){
                    $("#window-conversation").html(retorno.html); 
                    $('#send-message').hide();                  
                    $('#message, button').show();
                }
            }
        ); 
        
        $('#window-conversation').animate({ scrollTop: 60000 }, 'slow'); 
    }
    
    // Verificando se o usuario ainda tem permissao de permanecer no sistema
    function verifyUser(){
        
        // Verificando se nova mensagem e atualizando na tela
        $.get(
            urlbase + '/../verifyUser/',
            null,
            function(data){ 
                 
                var retorno = jQuery.parseJSON( data );
                                
                if( !retorno.success ){
                    window.location.href = urlbase + '/../';
                }
            }
        ); 
    }
        
    
    // a cada 2000ms atualizo as mensagens
    setInterval( getMessage, 1500 );
    
    // a cada 4000ms verifico se o usuario ainda tem permissao de usar o sistema
    setInterval( verifyUser, 4000 );
        
});