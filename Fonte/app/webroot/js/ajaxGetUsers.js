$(document).ready(function() { 
    
    // Pegando os usuarios ativos no sistema
    function getUsers(){
 
        
        // Verificando se a novos usuarios
        $.get(
            urlbase + '/../listUsers/',
            null,
            function(data){ 
                 
                var retorno = jQuery.parseJSON( data );
                                
                if( retorno.success ){                    
                    $("#users").html(retorno.html);                    
                }
            }
        ); 
    }

    // a cada 2000ms atualizo as mensagens
    setInterval( getUsers, 2000 );    
        
});